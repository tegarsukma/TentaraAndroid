package com.media.citra.indoartha.tentara;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.preference.PreferenceManager;

import java.io.IOException;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by j on 02/06/2017.
 */

public class AsyncGet extends AsyncTask<String, Integer, String[]> {
    private AsyncResponse response = null;
    private Context ctx;
    private ProgressDialog dialog;
    private String[] kode;

    public AsyncGet(Context ctx, AsyncResponse response) {
        super();
        this.ctx = ctx;
        this.response = response;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        dialog = new ProgressDialog(ctx);
        dialog.setMessage("Memuat...");
        dialog.setCancelable(false);
        dialog.show();
    }

    @Override
    protected String[] doInBackground(String... params) {
        OkHttpClient client = new OkHttpClient();
        // ip address server
        SharedPreferences sp =  PreferenceManager.getDefaultSharedPreferences(ctx);
        System.out.println("packageName:"+ctx.getPackageName());
        int length = params.length;
//        if (!sp.getString("ip_address", "kosong").equals("kosong")) {
        String url = "http://"+sp.getString("ip_address", "192.168.43.167");
        Request[] request = new Request[length];

        kode = new String[length];
        for (int i=0; i<length;i++){
            request[i] = new Request.Builder()
                    // url dari api yang mau diambil
                    .url(url + "/tentara/backend/web/api/"+params[i])
                    .build();
            kode[i] = params[i];
        }
        String[] response = new String[length];
        System.out.println("params size:"+params.length);

        try {
            Response[] r = new Response[length];
            for (int i=0;i<length;i++){
                r[i] = client.newCall(request[i]).execute();
                response[i] = r[i].body().string();
            }
            return response;
        } catch (IOException e) {
            e.printStackTrace();
            response[0] = "gagal";
            return response;
        }
    }

    @Override
    protected void onPostExecute(String[] s) {
        super.onPostExecute(s);
        if (s[0].equals("gagal")){
            dialog.setMessage("Gagal");
            dialog.setCancelable(true);
        } else {
            response.onProcessFinish(s, kode);
            dialog.dismiss();
        }
    }

    interface AsyncResponse{
        void onProcessFinish(String[] output, String[] kode);
    }
}
